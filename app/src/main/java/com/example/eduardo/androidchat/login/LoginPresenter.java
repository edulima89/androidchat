package com.example.eduardo.androidchat.login;

import com.example.eduardo.androidchat.login.events.LoginEvent;

/**
 * Created by eduardo on 3/10/17.
 */

public interface LoginPresenter {
    void onCreate();
    void onDestroy();
    void checkForAuthenticateUser();
    void validateLogin(String email, String password);
    void registerNewUser(String email, String password);
    void onEventMainThread(LoginEvent event);
}

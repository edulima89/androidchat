package com.example.eduardo.androidchat.login;

/**
 * Created by eduardo on 3/10/17.
 */

public interface LoginInteractor {
    void checkSession();
    void doSignIn(String email, String password);
    void doSignUp(String email, String password);

}

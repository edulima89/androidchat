package com.example.eduardo.androidchat;

import android.app.Application;

import com.firebase.client.Firebase;

/**
 * Created by eduardo on 3/10/17.
 */

public class AndroidChatApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private void setupFirebase(){
        Firebase.setAndroidContext(this);
        Firebase.getDefaultConfig().setPersistenceEnabled(Boolean.TRUE);
    }
}

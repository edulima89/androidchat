package com.example.eduardo.androidchat.lib;

/**
 * Created by eduardo on 3/13/17.
 */

public interface EventBus {
    void register(Object subscriber);
    void unregister(Object subscriber);
    void post(Object event);
}

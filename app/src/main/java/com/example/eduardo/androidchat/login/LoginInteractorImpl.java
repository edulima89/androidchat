package com.example.eduardo.androidchat.login;

/**
 * Created by eduardo on 3/13/17.
 */

public class LoginInteractorImpl implements LoginInteractor {
    private LoginRepository loginRepository;

    public LoginInteractorImpl() {
        this.loginRepository = new LoginRepositoryImpl();
    }

    @Override
    public void checkSession() {
        loginRepository.checkSession();
    }

    @Override
    public void doSignIn(String email, String password) {
        loginRepository.signIn(email,password);
    }

    @Override
    public void doSignUp(String email, String password) {
        loginRepository.signUp(email,password);
    }
}

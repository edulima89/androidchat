package com.example.eduardo.androidchat.entities;

import java.util.Map;

/**
 * Created by eduardo on 3/13/17.
 */
public class User {
    String email;
    Boolean online;
    Map<String, Boolean> contacts;
    public final static Boolean ONLINE= Boolean.TRUE;
    public final static Boolean OFFLINE= Boolean.FALSE;

    public User() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    public Map<String, Boolean> getContacts() {
        return contacts;
    }

    public void setContacts(Map<String, Boolean> contacts) {
        this.contacts = contacts;
    }
}

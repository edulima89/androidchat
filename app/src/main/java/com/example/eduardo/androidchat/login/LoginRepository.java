package com.example.eduardo.androidchat.login;

/**
 * Created by eduardo on 3/10/17.
 */

public interface LoginRepository {
    void signUp(String email, String password);
    void signIn(String email, String password);
    void checkSession();
}

package com.example.eduardo.androidchat.login;

/**
 * Created by eduardo on 3/10/17.
 */

public interface LoginView {
    void enableInputs();
    void disableInputs();
    void showProgress();
    void hideProgress();

    void handleSignUp();
    void handleSignIn();

    void navigateMainScreen();
    void loginError(String error);

    void newUserSuccess();
    void newUserError(String error);
}

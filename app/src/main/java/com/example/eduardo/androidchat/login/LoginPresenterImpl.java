package com.example.eduardo.androidchat.login;

import android.util.Log;

import com.example.eduardo.androidchat.lib.EventBus;
import com.example.eduardo.androidchat.lib.GreenRobotEventBus;
import com.example.eduardo.androidchat.login.events.LoginEvent;

/**
 * Created by eduardo on 3/13/17.
 */

public class LoginPresenterImpl implements LoginPresenter{
    private EventBus eventBus;
    private LoginView loginview;
    private LoginInteractor loginInterctor;

    public LoginPresenterImpl(LoginView loginView){
        this.loginview = loginView;
        this.loginInterctor = new LoginInteractorImpl();
        this.eventBus= GreenRobotEventBus.getInstance();
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        loginview = null;
        eventBus.unregister(this);
    }

    @Override
    public void checkForAuthenticateUser() {
        if(loginview != null){
            loginview.disableInputs();
            loginview.showProgress();
        }
        loginInterctor.checkSession();
    }

    @Override
    public void validateLogin(String email, String password) {
        if(loginview != null){
            loginview.disableInputs();
            loginview.showProgress();
        }
        loginInterctor.doSignIn(email,password);
    }

    @Override
    public void registerNewUser(String email, String password) {
        if(loginview != null){
            loginview.disableInputs();
            loginview.showProgress();
        }
        loginInterctor.doSignUp(email,password);
    }

    @Override
    public void onEventMainThread(LoginEvent event) {
        switch (event.getEventType()){
            case LoginEvent.onSignInSuccess:
                onSignInSuccess();
                break;
            case LoginEvent.onSignUpSuccess:
                onSignInSuccess();
                break;
            case LoginEvent.onSignInError:
                onSignInError(event.getErrorMessage());
                break;
            case LoginEvent.onSignUpError:
                onSignUpError(event.getErrorMessage());
                break;
            case LoginEvent.onFailedToRecoverSession:
                onFailedToRecoverSession();
                break;
        }
    }

    private void onFailedToRecoverSession() {
        if(loginview != null) {
            loginview.hideProgress();
            loginview.enableInputs();
        }
        Log.e("LoginPresenterImpl", "onFailedToRecoverSession");
    }

    private void onSignInSuccess(){
        if(loginview != null)
            loginview.navigateMainScreen();
    }

    private void onSignUpSuccess(){
        if(loginview != null)
            loginview.newUserSuccess();
    }

    private void onSignInError(String error){
        if(loginview != null) {
            loginview.hideProgress();
            loginview.enableInputs();
            loginview.loginError(error);
        }
    }

    private void onSignUpError(String error){
        if(loginview != null) {
            loginview.hideProgress();
            loginview.enableInputs();
            loginview.newUserError(error);
        }

    }



}
